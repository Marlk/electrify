package com.recytecreations.electrify;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.recytecreations.electrify.api.AccountApi;
import com.recytecreations.electrify.api.EventApi;
import com.recytecreations.electrify.models.SearchResult;

import java.util.ArrayList;

public class QuickStartActivity extends AppCompatActivity {

    ViewFlipper viewFlipper;
    EditText search;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quick_start);

        viewFlipper = (ViewFlipper) findViewById(R.id.viewFlipper);
        search = (EditText) findViewById(R.id.event);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter.setRecyclerView(recyclerView);

        viewFlipper.setInAnimation(this, R.anim.slide_in_right);
        viewFlipper.setOutAnimation(this, R.anim.slide_out_left);
        findViewById(R.id.next).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewFlipper.showNext();
            }
        });

        search.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    search.addTextChangedListener(textWatcher);
                } else {
                    search.removeTextChangedListener(textWatcher);
                }
            }
        });

        findViewById(R.id.done).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = ((EditText) findViewById(R.id.username)).getText().toString();
                AccountApi.createAccount(QuickStartActivity.this, username, chosenEvent.getId(), chosenEvent.getTitle(), new AccountApi.accountResult() {
                    @Override
                    public void success() {
                        startActivity(new Intent(QuickStartActivity.this, MainActivity.class));
                    }

                    @Override
                    public void failure(String error) {
                        Toast.makeText(QuickStartActivity.this, error, Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }

    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (s.length() == 0)
                return;

            EventApi.searchEvents(QuickStartActivity.this, new EventApi.searchResult() {
                @Override
                public void success(ArrayList<SearchResult> results) {
                    adapter.clear();
                    adapter.addAll(results);
                }

                @Override
                public void failure() {
                    Toast.makeText(QuickStartActivity.this, "Error searching events", Toast.LENGTH_LONG).show();
                }
            });
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    SearchResult chosenEvent;
    EasyAdapter<SearchResult> adapter = new EasyAdapter<SearchResult>() {
        @Override
        public ModelView getView() {
            return new ModelView() {
                TextView title;
                TextView description;
                TextView location;
                SearchResult model;

                @Override
                public View getView(ViewGroup parent) {
                    final View rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_search_result, parent, false);

                    title = (TextView) rootView.findViewById(R.id.title);
                    description = (TextView) rootView.findViewById(R.id.description);
                    location = (TextView) rootView.findViewById(R.id.location);

                    rootView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            chosenEvent = model;
                            search.removeTextChangedListener(textWatcher);
                            search.setText(model.getTitle());
                            adapter.clear();
                        }
                    });

                    return rootView;
                }

                @Override
                public void bindModel(SearchResult model, int index) {
                    title.setText(model.getTitle());
                    description.setText(model.getDescription());
                    location.setText(model.getLocation());
                    this.model = model;
                }
            };
        }
    };
}
