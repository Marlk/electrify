package com.recytecreations.electrify.models;

/**
 * Created by Allan on 26/09/2016.
 */
public class ScoreboardUser {
    public String name;
    public int points;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }
}
