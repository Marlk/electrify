package com.recytecreations.electrify.models;

/**
 * Created by Allan on 17/09/2016.
 */
public class Item {
    public String title;
    public int points;

    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }
}
