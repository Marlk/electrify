package com.recytecreations.electrify;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Collection;

/**
 * This is a wrapper for a RecyclerView adapter that makes
 * it as simple as possible to implement the view holder pattern
 * @param <M>
 */
public abstract class EasyAdapter<M> extends ArrayList<M> {
    // The internal adapter that the recyclerview will directly use
    InternalAdapter internalAdapter = new InternalAdapter();

    /// This returns the wrapped RecyclerView adapter which can be set as the adapter for
    // a recycler view
    public RecyclerView.Adapter getRecyclerViewAdapater(){
        return internalAdapter;
    }
    // This will setup this adapter as the adatper of the supplied recycler view
    public void setRecyclerView(RecyclerView recyclerView){
        recyclerView.setAdapter(internalAdapter);
    }


    //  -------------     Wrapped from array list        -----------------------
    // The following methods are all wrapped from the array list,
    // but they call the underlying recyclerview adapter update methods - which
    // makes this a live updating ArrayList adapter
    @Override
    public boolean add(M object) {
        boolean result = super.add(object);

        internalAdapter.notifyItemInserted(indexOf(object));

        return result;
    }

    @Override
    public void add(int index, M object) {
        super.add(index, object);
        internalAdapter.notifyItemInserted(index);
    }

    @Override
    public boolean addAll(Collection<? extends M> collection) {
        boolean result = super.addAll(collection);
        internalAdapter.notifyDataSetChanged();
        return result;
    }

    @Override
    public boolean addAll(int index, Collection<? extends M> collection) {
        boolean result = super.addAll(index, collection);
        internalAdapter.notifyDataSetChanged();
        return result;
    }

    @Override
    public void clear() {
        super.clear();
        internalAdapter.notifyDataSetChanged();
    }

    @Override
    public M remove(int index) {
        M result = super.remove(index);

        internalAdapter.notifyItemRemoved(index);

        return result;
    }

    @Override
    public boolean remove(Object object) {
        int index = indexOf(object);
        boolean result = super.remove(object);

        internalAdapter.notifyItemRemoved(index);

        return result;
    }

    @Override
    protected void removeRange(int fromIndex, int toIndex) {
        super.removeRange(fromIndex, toIndex);

        internalAdapter.notifyDataSetChanged();
    }

    @Override
    public M set(int index, M object) {
        M result =  super.set(index, object);

        internalAdapter.notifyItemChanged(index);

        return result;
    }

    /**
     * Tell the adapter that the data set has changed, and so all views should be reloaded
     */
    public void notifyDatasetChanged(){
        internalAdapter.notifyDataSetChanged();
    }
    public void notifyItemInserted(int index){
        internalAdapter.notifyItemInserted(index);
    }
    public void notifyItemRemoved(int index){
        internalAdapter.notifyItemRemoved(index);
    }

    //  -------------     END Wrapped from array list        -----------------------

    // This internal class is the actual recycler view adapter which is wrapped with generics,
    // which simplifies its use
    class InternalAdapter extends RecyclerView.Adapter<InternalAdapter.ViewHolder>{
        public class ViewHolder extends RecyclerView.ViewHolder{
            android.view.View rootView;
            ModelView modelView;
            public ViewHolder(android.view.View itemView, ModelView modelView) {
                super(itemView);
                this.modelView = modelView;
                rootView = itemView;
            }
            public ModelView getModelView(){
                return modelView;
            }
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            ModelView modelViewModel = getView();
            android.view.View view = modelViewModel.getView(parent);
            ViewHolder viewHolder = new ViewHolder(view, modelViewModel);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            holder.getModelView().bindModel(get(position), position);
        }

        @Override
        public int getItemCount() {
            return size();
        }
    }

    /**
     * This is called when the adapter wants a new view to use to display a model
     * @return A new ModelView which is capable of diplaying a model
     */
    public abstract ModelView getView();

    public abstract class ModelView {
        /**
         * This should be called when the underlying data model for this view gets changed,
         * and the view should be rebound to the data model
         */
        public void invalidate(){
            throw new RuntimeException("Not implemented");
        }

        /**
         * This is called when the view is created
         * @return A view that will represent the data model
         */
        public abstract View getView(ViewGroup parent);

        /**
         * This gets called when this view should be bound to the specified model
         * @param model
         */
        public abstract void bindModel(M model, int index);
    }
}
