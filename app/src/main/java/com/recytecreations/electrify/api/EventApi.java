package com.recytecreations.electrify.api;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import com.recytecreations.electrify.R;
import com.recytecreations.electrify.models.ScoreboardUser;
import com.recytecreations.electrify.models.SearchResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Allan on 23/09/2016.
 */
public class EventApi {
    public static void searchEvents(final Context context, final searchResult callback) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    OkHttpClient client = new OkHttpClient.Builder()
                            .connectTimeout(10, TimeUnit.SECONDS)
                            .writeTimeout(10, TimeUnit.SECONDS)
                            .readTimeout(10, TimeUnit.SECONDS)
                            .build();

                    Request get = new Request.Builder()
                            .addHeader("Content-Type", "application/json")
                            .url(context.getString(R.string.base_endpoint) + "/event/get")
                            .build();

                    final Response response = client.newCall(get).execute();
                    final String responseString = response.body().string().toString();
                    if (response.code() >= 200 && response.code() < 300) {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    ArrayList<SearchResult> searchResults = new ArrayList<>();
                                    JSONArray events = new JSONArray(responseString);

                                    for (int i = 0; i < events.length(); i++){
                                        SearchResult result = new SearchResult();
                                        JSONObject jsonResult = new JSONObject(events.getString(i));

                                        result.setId(jsonResult.getString("Id"));
                                        result.setTitle(jsonResult.getString("Name"));
                                        result.setDescription(jsonResult.getString("Description"));
                                        result.setLocation(jsonResult.getString("Location"));

                                        searchResults.add(result);
                                    }
                                    callback.success(searchResults);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    callback.failure();
                                }
                            }
                        });
                    }
                    else {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                callback.failure();
                            }
                        });
                    }
                } catch (Exception e) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.failure();
                        }
                    });
                }
            }
        }).start();
    }

    public interface searchResult{
        void success(ArrayList<SearchResult> results);
        void failure();
    }

    public static void getStats(final Context context, final statsResult callback) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    OkHttpClient client = new OkHttpClient.Builder()
                            .connectTimeout(10, TimeUnit.SECONDS)
                            .writeTimeout(10, TimeUnit.SECONDS)
                            .readTimeout(10, TimeUnit.SECONDS)
                            .build();

                    Request get = new Request.Builder()
                            .addHeader("Content-Type", "application/json")
                            .url(context.getString(R.string.base_endpoint) + "/event/getStats?eventId=" + context.getSharedPreferences("Settings", Context.MODE_PRIVATE).getString("eventId", null))
                            .build();

                    final Response response = client.newCall(get).execute();
                    final String responseString = response.body().string().toString();
                    if (response.code() >= 200 && response.code() < 300) {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    ArrayList<ScoreboardUser> statsResults = new ArrayList<>();
                                    JSONArray events = new JSONArray(responseString);

                                    for (int i = 0; i < events.length(); i++){
                                        ScoreboardUser result = new ScoreboardUser();
                                        JSONObject jsonResult = new JSONObject(events.getString(i));

                                        result.setName(jsonResult.getString("Name"));
                                        result.setPoints(jsonResult.getInt("Points"));

                                        statsResults.add(result);
                                    }
                                    callback.success(statsResults);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    callback.failure();
                                }
                            }
                        });
                    }
                    else {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                callback.failure();
                            }
                        });
                    }
                } catch (Exception e) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.failure();
                        }
                    });
                }
            }
        }).start();
    }

    public interface statsResult{
        void success(ArrayList<ScoreboardUser> results);
        void failure();
    }
}
