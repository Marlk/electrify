package com.recytecreations.electrify.api;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import com.recytecreations.electrify.R;

import java.util.concurrent.TimeUnit;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Allan on 24/09/2016.
 */
public class AccountApi {
    public static void createAccount(final Context context, final String username, final String eventId, final String eventName, final accountResult callback) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    OkHttpClient client = new OkHttpClient.Builder()
                            .connectTimeout(10, TimeUnit.SECONDS)
                            .writeTimeout(10, TimeUnit.SECONDS)
                            .readTimeout(10, TimeUnit.SECONDS)
                            .build();

                    RequestBody formBody = new FormBody.Builder()
                            .add("username", username)
                            .add("eventId", eventId)
                            .build();

                    Request post = new Request.Builder()
                            .addHeader("Content-Type", "application/json")
                            .url(context.getString(R.string.base_endpoint) + "/user/create")
                            .post(formBody)
                            .build();

                    final Response response = client.newCall(post).execute();
                    final String responseString = response.body().string().toString();
                    if (response.code() >= 200 && response.code() < 300) {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                if(Integer.parseInt(responseString) == 0){
                                    context.getSharedPreferences("Settings", Context.MODE_PRIVATE).edit()
                                            .putString("username", username)
                                            .putString("endDate", "25/9/2016")
                                            .putString("eventName", eventName)
                                            .putString("eventId", eventId)
                                            .apply();
                                    callback.success();
                                }
                                else if (Integer.parseInt(responseString) == 2)
                                {
                                    callback.failure("Someone at this event is already using this username.");
                                }
                                else {
                                    callback.failure("This event does not exist.");
                                }
                            }
                        });
                    }
                    else {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                callback.failure(responseString);
                            }
                        });
                    }
                } catch (Exception e) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.failure("Connection Error");
                        }
                    });
                }
            }
        }).start();
    }

    public interface accountResult{
        void success();
        void failure(String error);
    }
}
