package com.recytecreations.electrify.api;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Looper;

import com.recytecreations.electrify.R;
import com.recytecreations.electrify.models.Item;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Allan on 25/09/2016.
 */
public class ItemApi {
    public static void getItems(final Context context, final itemResult callback) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    OkHttpClient client = new OkHttpClient.Builder()
                            .connectTimeout(10, TimeUnit.SECONDS)
                            .writeTimeout(10, TimeUnit.SECONDS)
                            .readTimeout(10, TimeUnit.SECONDS)
                            .build();

                    SharedPreferences sharedPreferences = context.getSharedPreferences("Settings", Context.MODE_PRIVATE);
                    Request get = new Request.Builder()
                            .addHeader("Content-Type", "application/json")
                            .url(context.getString(R.string.base_endpoint) + "/item/get?eventId=" + sharedPreferences.getString("eventId", null) + "&username=" + sharedPreferences.getString("username", null))
                            .build();

                    final Response response = client.newCall(get).execute();
                    final String responseString = response.body().string().toString();
                    if (response.code() >= 200 && response.code() < 300) {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    ArrayList<Item> items = new ArrayList<>();
                                    JSONArray events = new JSONArray(responseString);

                                    for (int i = 0; i < events.length(); i++){
                                        Item result = new Item();
                                        JSONObject jsonResult = new JSONObject(events.getString(i));

                                        result.setTitle(jsonResult.getString("Name"));
                                        result.setPoints(jsonResult.getInt("Points"));

                                        items.add(result);
                                    }
                                    callback.success(items);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    callback.failure();
                                }
                            }
                        });
                    }
                    else {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                callback.failure();
                            }
                        });
                    }
                } catch (Exception e) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.failure();
                        }
                    });
                }
            }
        }).start();
    }

    public static void redeemItem(final Context context, final String itemCode, final redeemItemResult callback) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    OkHttpClient client = new OkHttpClient.Builder()
                            .connectTimeout(10, TimeUnit.SECONDS)
                            .writeTimeout(10, TimeUnit.SECONDS)
                            .readTimeout(10, TimeUnit.SECONDS)
                            .build();

                    SharedPreferences sharedPreferences = context.getSharedPreferences("Settings", Context.MODE_PRIVATE);

                    RequestBody formBody = new FormBody.Builder()
                            .add("username", sharedPreferences.getString("username", null))
                            .add("eventId", sharedPreferences.getString("eventId", null))
                            .add("itemCode", itemCode)
                            .build();

                    Request post = new Request.Builder()
                            .addHeader("Content-Type", "application/json")
                            .url(context.getString(R.string.base_endpoint) + "/item/post")
                            .post(formBody)
                            .build();

                    final Response response = client.newCall(post).execute();
                    final String responseString = response.body().string().toString();
                    if (response.code() >= 200 && response.code() < 300) {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                if (Integer.parseInt(responseString) == 0){
                                    callback.success();
                                }
                                else if (Integer.parseInt(responseString) == 2){
                                    callback.failure("You already have this item");
                                }
                                else {
                                    callback.failure("Item not found");
                                }
                            }
                        });
                    }
                    else {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                callback.failure("Unknown Error");
                            }
                        });
                    }
                } catch (Exception e) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.failure("No connection");
                        }
                    });
                }
            }
        }).start();
    }

    public interface redeemItemResult{
        void success();
        void failure(String error);
    }

    public interface itemResult{
        void success(ArrayList<Item> results);
        void failure();
    }
}
