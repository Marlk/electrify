package com.recytecreations.electrify;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        SharedPreferences sharedPreferences = getSharedPreferences("Settings", MODE_PRIVATE);

//        if (sharedPreferences.getString("endDate", null) != null){
//            try {
//                SimpleDateFormat dateFormat = new SimpleDateFormat("DD/MM/YYYY");
//                dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
//                Date endDate = dateFormat.parse(sharedPreferences.getString("endDate", null));
//
//
//                if (endDate.getTime() < System.currentTimeMillis()){
//                    //sharedPreferences.edit().clear().apply();
                    //startActivity(new Intent(this, QuickStartActivity.class));
//                }
//                else {
//                    startActivity(new Intent(this, MainActivity.class));
//                }
//            } catch (ParseException e) {
//                startActivity(new Intent(this, QuickStartActivity.class));
//                e.printStackTrace();
//            }
//        }
//        else {
//            startActivity(new Intent(this, QuickStartActivity.class));
//        }
        startActivity(new Intent(this, MainActivity.class));
    }
}
