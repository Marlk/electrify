package com.recytecreations.electrify;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.recytecreations.electrify.api.EventApi;
import com.recytecreations.electrify.api.ItemApi;
import com.recytecreations.electrify.models.Item;
import com.recytecreations.electrify.models.ScoreboardUser;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    View addItemView;
    Button redeemButton;
    FloatingActionButton actionButton;
    RecyclerView recyclerView;
    RecyclerView scoreboardRecyclerView;
    SwipeRefreshLayout refreshLayout;
    SwipeRefreshLayout scoreboardRefresh;
    EditText redeemCode;
    View mask;
    ViewFlipper viewFlipper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        getSupportActionBar().setTitle(getSharedPreferences("Settings", MODE_PRIVATE).getString("eventName", null));

        addItemView = findViewById(R.id.addItem);
        actionButton = (FloatingActionButton) findViewById(R.id.floatinActionButton);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        refreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh);
        redeemButton = (Button) findViewById(R.id.redeem);
        redeemCode = (EditText) findViewById(R.id.redeemCode);
        mask = findViewById(R.id.mask);
        viewFlipper = (ViewFlipper) findViewById(R.id.viewFlipper);
        scoreboardRecyclerView = (RecyclerView) findViewById(R.id.scoreBoardRecyclerView);
        scoreboardRefresh = (SwipeRefreshLayout) findViewById(R.id.scoreBoardRefresh);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter.setRecyclerView(recyclerView);
        scoreboardRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        scoreboardAdapter.setRecyclerView(scoreboardRecyclerView);

        addItemView.setVisibility(View.GONE);

        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMenu();
            }
        });

        getItems();
        getStats();

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getItems();
            }
        });

        scoreboardRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getStats();
            }
        });

        redeemButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (redeemCode.getText().toString() == null)
                    return;
                ItemApi.redeemItem(MainActivity.this, redeemCode.getText().toString(), new ItemApi.redeemItemResult() {
                    @Override
                    public void success() {
                        Toast.makeText(MainActivity.this, "Item Collected!", Toast.LENGTH_SHORT).show();
                        hideMenu();
                        getItems();
                    }

                    @Override
                    public void failure(String error) {
                        Toast.makeText(MainActivity.this, error, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });


        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideMenu();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (viewFlipper.getDisplayedChild() == 0){
            getMenuInflater().inflate(R.menu.options_menu, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.scoreBoard){
            viewFlipper.setDisplayedChild(1);
            getSupportActionBar().setTitle("Scoreboard");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            invalidateOptionsMenu();
        }
        else if (item.getItemId() == android.R.id.home){
            viewFlipper.setDisplayedChild(0);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setTitle(getSharedPreferences("Settings", MODE_PRIVATE).getString("eventName", null));
            invalidateOptionsMenu();
        }
        return super.onOptionsItemSelected(item);
    }

    private void hideKeyboard(){
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private void hideMenu(){
        Animation fadeAnimation = AnimationUtils.loadAnimation(MainActivity.this, R.anim.fade_out);
        mask.startAnimation(fadeAnimation);
        mask.setClickable(false);
        fadeAnimation.setFillAfter(true);

        actionButton.show();
        hideKeyboard();
        Animation animation = AnimationUtils.loadAnimation(MainActivity.this, R.anim.slide_out_top);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                addItemView.setVisibility(View.GONE);
                actionButton.show();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        addItemView.startAnimation(animation);
    }

    private void showMenu(){
        redeemCode.setText("");
        Animation fadeAnimation = AnimationUtils.loadAnimation(MainActivity.this, R.anim.fade_in);
        fadeAnimation.setFillAfter(true);
        mask.setBackgroundColor(Color.parseColor("#c3c3c3"));
        mask.startAnimation(fadeAnimation);
        mask.setClickable(true);

        if (addItemView.getVisibility() == View.VISIBLE)
            return;

        actionButton.hide();
        addItemView.setVisibility(View.VISIBLE);
        Animation animation = AnimationUtils.loadAnimation(MainActivity.this, R.anim.slide_in_bottom);
        addItemView.startAnimation(animation);
    }

    private void getItems(){
        ItemApi.getItems(this, new ItemApi.itemResult() {
            @Override
            public void success(ArrayList<Item> results) {
                adapter.clear();
                adapter.addAll(results);
                refreshLayout.setRefreshing(false);
            }

            @Override
            public void failure() {
                Toast.makeText(MainActivity.this, "Couldn't connect", Toast.LENGTH_SHORT).show();
                refreshLayout.setRefreshing(false);
            }
        });
    }

    private void getStats(){
        EventApi.getStats(this, new EventApi.statsResult() {
            @Override
            public void success(ArrayList<ScoreboardUser> results) {
                scoreboardAdapter.clear();
                scoreboardAdapter.addAll(results);
                scoreboardRefresh.setRefreshing(false);
            }

            @Override
            public void failure() {
                Toast.makeText(MainActivity.this, "Couldn't connect", Toast.LENGTH_SHORT).show();
                scoreboardRefresh.setRefreshing(false);
            }
        });
    }

    EasyAdapter<Item> adapter = new EasyAdapter<Item>() {
        @Override
        public ModelView getView() {
            return new ModelView() {
                TextView title;
                TextView points;

                @Override
                public View getView(ViewGroup parent) {
                    final View rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_item, parent, false);

                    title = (TextView) rootView.findViewById(R.id.title);
                    points = (TextView) rootView.findViewById(R.id.points);

                    return rootView;
                }

                @Override
                public void bindModel(Item model, int index) {
                    title.setText(model.getTitle());
                    points.setText("Points: " + model.getPoints());
                }
            };
        }
    };

    EasyAdapter<ScoreboardUser> scoreboardAdapter = new EasyAdapter<ScoreboardUser>() {
        @Override
        public ModelView getView() {
            return new ModelView() {
                TextView name;
                TextView points;

                @Override
                public View getView(ViewGroup parent) {
                    final View rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_scoreboard, parent, false);

                    name = (TextView) rootView.findViewById(R.id.name);
                    points = (TextView) rootView.findViewById(R.id.points);

                    return rootView;
                }

                @Override
                public void bindModel(ScoreboardUser model, int index) {
                    if (model.getName().equals(MainActivity.this.getSharedPreferences("Settings", MODE_PRIVATE).getString("username", null))){
                        ((View)name.getParent()).setBackgroundColor(Color.parseColor("#4caf50"));
                    }
                    else {
                        ((View)name.getParent()).setBackgroundColor(Color.TRANSPARENT);
                    }
                    name.setText(model.getName());
                    points.setText("Points: " + model.getPoints());
                }
            };
        }
    };
}
